package com.lecuong.repository;

import com.lecuong.paging.Pageable;

import java.util.Optional;
import java.util.stream.Stream;

public interface JpaRepository<T, ID> {

    T save(T t);

    void update(ID id, T t);

    Optional<T> findById(ID id);

    void delete(ID id);

    Stream<T> findAll();

    Stream<T> findAll(Pageable pageable);
}
