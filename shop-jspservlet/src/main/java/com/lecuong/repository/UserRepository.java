package com.lecuong.repository;

import com.lecuong.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
}
