package com.lecuong.utils;

import com.lecuong.constant.annotation.Column;
import com.lecuong.constant.annotation.Entity;
import com.lecuong.constant.annotation.Id;

import java.lang.reflect.Method;
import java.util.Locale;

public class ReflectionUtils {

    public static String getColumnName(Class<?> clazz, String fieldName){
        try {
            return clazz.getDeclaredField(fieldName).getAnnotation(Column.class).value();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static String primaryName(Class<?> clazz, String fieldName){
        try {
            return clazz.getDeclaredField(fieldName).getAnnotation(Id.class).value();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return "";
        }
    }

    public static boolean autoIncrement(Class<?> clazz, String fieldName){
        try {
            return clazz.getDeclaredField(fieldName).getAnnotation(Id.class).autoIncrement();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getClassName(Class<?> clazz){
        return clazz.getAnnotation(Entity.class).value();
    }

    public static Object methodGet(Object object, String fieldName){
        StringBuilder methodName = new StringBuilder("get")
                .append(fieldName.substring(0, 1).toUpperCase())
                .append(fieldName.substring(1));

        try {
            Method method = object.getClass().getDeclaredMethod(methodName.toString());
            return method.invoke(object);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
