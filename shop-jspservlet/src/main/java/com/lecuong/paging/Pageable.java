package com.lecuong.paging;

public interface Pageable {

    int getIndex();

    int getSize();

    int getOffset();
}
