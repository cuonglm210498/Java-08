package com.lecuong;

import com.lecuong.connection.MySQLConnection;
import com.lecuong.constant.Config;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {

        try {
            FileReader fileReader = new FileReader(System.getProperty("user.dir") + "/src/main/resources/config.properties");
            Properties properties = new Properties();
            properties.load(fileReader);
            Config.url = properties.getProperty("url");
            Config.username = properties.getProperty("username");
            Config.password = properties.getProperty("password");
            Config.driverClassName = properties.getProperty("driverClassName");
            Config.maxPoolSize = Integer.parseInt(properties.getProperty("maxPoolSize"));
            Config.connectionTimeOut = Integer.parseInt(properties.getProperty("connectionTimeOut"));

            Connection connection = MySQLConnection.connection();

            String sql = "SELECT * FROM products";
            PreparedStatement ps = connection.prepareStatement(sql);

            ResultSet rs = ps.executeQuery();

            while(rs.next()){
                System.out.printf("ID %4s Name %4s", rs.getString("id"), rs.getString("name"));

            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
