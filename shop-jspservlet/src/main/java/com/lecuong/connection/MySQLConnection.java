package com.lecuong.connection;

import com.lecuong.constant.Config;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySQLConnection {

    public static Connection connection(){
        try {
            return ConnectionPool.getDataSource().getConnection();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }

    }

}
