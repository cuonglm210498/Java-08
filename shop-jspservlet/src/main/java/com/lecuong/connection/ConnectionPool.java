package com.lecuong.connection;

import com.lecuong.constant.Config;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

public class ConnectionPool {

    private volatile static HikariConfig config;
    private volatile static HikariDataSource dataSource;

    static {
        config = new HikariConfig();
        config.setJdbcUrl(Config.url);
        config.setUsername(Config.username);
        config.setPassword(Config.password);
        config.setDriverClassName(Config.driverClassName);
        config.setMaximumPoolSize(Config.maxPoolSize);
        config.setConnectionTimeout(Config.connectionTimeOut * 1000);
        dataSource = new HikariDataSource(config);
    }

    public static HikariDataSource getDataSource() {
        return dataSource;
    }
}
