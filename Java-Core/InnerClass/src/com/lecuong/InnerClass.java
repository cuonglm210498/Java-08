package com.lecuong;

public class InnerClass {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public class Inner {
        public void print() {
            System.out.println(getName());
        }
    }
}
