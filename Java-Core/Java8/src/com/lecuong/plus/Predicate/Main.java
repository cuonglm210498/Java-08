package com.lecuong.plus.Predicate;

import java.util.function.Predicate;

public class Main {

    public static void main(String[] args) {
        Predicate<String> predicate = s -> s.isEmpty();
        System.out.println(predicate.test(""));
    }
}
