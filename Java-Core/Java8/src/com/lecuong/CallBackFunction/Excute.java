package com.lecuong.CallBackFunction;

public class Excute {

    private String s;

    public void execute(CallBack callBack, String m) {
        callBack.call(m);
        fake(m);
    }

    public void fake(String m) {
        System.out.println("After callback: " + m);
    }
}
