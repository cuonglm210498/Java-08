package com.lecuong.ReferenceMethod;

public class Main {

    public static void main(String[] args) {

        Expression expression = (a, b) -> a + b;
        int tong = tinhToan(1, 2, expression);

        //int tong = tinhToan(1, 2, (a, b) -> tinhTong(a,b));
        //int tong = tinhToan(1, 2, Main::tinhTong);

        int hieu = tinhToan(5,2, Main::tinhHieu);

        System.out.println(tong);

        System.out.println(hieu);
    }

    public static int tinhToan(int a, int b, Expression expression) {
        return expression.expression(a, b);
    }

    public static int tinhTong(int a, int b) {
        return a + b;
    }

    public static int tinhHieu(int a, int b) {
        return a - b;
    }
}
