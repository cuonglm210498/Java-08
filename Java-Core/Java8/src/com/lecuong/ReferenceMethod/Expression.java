package com.lecuong.ReferenceMethod;

@FunctionalInterface
public interface Expression {

    int expression(int a, int b);
}
