package com.lecuong.DefaultMethod;

public class Main {

    /*
    * - Từ java 7 trở về trước
    *   trong interface các method bắt buộc là abtract method (các phương thức không có thân hàm)
    *   => Vì vậy mới sinh ra adapter để tránh việc khi mà 1 interface bổ sung thêm 1 phương thức
    *      thì tất cả các class implements phải override lại.
    *   => Giả sử chúng ta đang có 100 class implements interface này. Khi interface này bổ sung thêm
    *      thêm một phương thức thì 100 class này phải override lại phương thức này mặc dù có những class
    *      không dùng đến hàm đó. (Nó vi phạm nguyên lý thứ 2 trong nguyên lý SOLID).
    *   => Giải pháp:
    *      + 1 cái interface chứa các method
    *      + 1 class Adapter implements interface
    *      + 100 class extends lại class Adapter
    *      + Giải pháp này là đúng cho dù interface có thêm 10 method nữa thì các class implements sẽ không ảnh hưởng gì.
    *
    * - Từ java 8 thằng interface cho phép khai báo các method có thân hàm nhưng phải sử dụng từ khóa default trước mỗi hàm đó
    *   và khi method được khai báo là default thì mặc định các class implements sẽ không phải override lại method mà mặc định được sử dụng
    *   + static method thường được dùng để khởi tạo đối tượng.
    */

    public static void main(String[] args) {
        Vehicle car = new Car();
        car.print();
        car.showLong();
    }
}
