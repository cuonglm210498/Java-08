package com.lecuong.DefaultMethod;

public interface Vehicle {

    default void print() {
        System.out.println("Vehicle printed");
    }

    void showLong();
}
