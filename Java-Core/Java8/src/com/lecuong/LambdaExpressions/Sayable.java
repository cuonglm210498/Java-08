package com.lecuong.LambdaExpressions;

@FunctionalInterface
public interface Sayable {

    String say();
}
