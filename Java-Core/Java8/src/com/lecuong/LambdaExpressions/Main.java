package com.lecuong.LambdaExpressions;

public class Main {

    public static void main(String[] args) {

        // (Danh sách tham số) -> {thực thi}
        Sum sum = (a, b) -> { return a + b; };
        System.out.println(sum.sum(1,2));

        Sayable sayable = () -> { return "a"; };
        System.out.println(sayable.say());

        Drawable drawable = () -> {
            System.out.println("abc");
        };
        drawable.draw();

        Test test = (a, b) -> {
            System.out.println(a + b);
        };
        test.test(1,2);
    }
}
