package com.lecuong.LambdaExpressions;

@FunctionalInterface
public interface Drawable {

    void draw();
}
