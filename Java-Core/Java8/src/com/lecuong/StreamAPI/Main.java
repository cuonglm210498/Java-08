package com.lecuong.StreamAPI;

import java.util.Arrays;
import java.util.Optional;
import java.util.OptionalInt;

public class Main {

    public static void main(String[] args) {

        int[] arr = new int[]{1,2,3,4,5,6,7,8,9,10};

        Arrays.stream(arr)
                .filter(s -> s > 5)
                .forEach(s -> System.out.println(s));

        Arrays.stream(arr)
                .filter(s -> s > 5)
                .map(s -> s * 2)
                .forEach(System.out::println);

        OptionalInt result = Arrays.stream(arr)
                .filter(s -> s == 5)
                .findFirst();

        Arrays.stream(arr)
                .sorted().forEach(System.out::println);

        int max = Arrays.stream(arr)
                .max()
                .getAsInt();
        System.out.println(max);

        int min = Arrays.stream(arr)
                .min()
                .getAsInt();
        System.out.println(min);

        int sum = Arrays.stream(arr)
                .reduce(0, (s, i) -> s + i);
        System.out.println(sum);
    }
}
