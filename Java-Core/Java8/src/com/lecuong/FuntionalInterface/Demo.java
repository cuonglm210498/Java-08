package com.lecuong.FuntionalInterface;

@FunctionalInterface
public interface Demo<T> {

    boolean test(T t);

}
