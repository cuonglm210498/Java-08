package com.lecuong;

public class Main {

    /*
    * Final:
    *       + Dùng để  khai báo một hằng số trong java.
    *       + Khi 1 biến được khai báo là final thì chúng ta không thể thay đổi giá trị của nó.
    *       + Có thể không khởi tạo giá trị cho biến final nhưng chúng ta phải khởi tạo nó trong constructor, và chỉ khởi tạo 1 lần.
    *       + Khi một class khai báo là final thì các class khác sẽ không kế thừa được class đó.
    *       + Khi một hàm được khai báo là final thì hàm đó sẽ không thể bị Override.
    *  Static (biến - phương thức - class - static block):
    *       + Trong java có 2 phạm vi biến:
    *           - Biến instance (non-static):
    *           - Biến class (static):
    *                   - Biến được khai báo là static thì biến đó chỉ được khởi tạo đúng 1 lần.
    *                   - Biến đó được truy cập thông qua tên class không cần thông qua object.
    *                   - Biến static thì mọi object có thể dùng chung.
    *
    * Trong java chúng ta có hai thời gian:
    *       + Compiler:
    *           - Khi 1 biến hay 1 method được khai báo là static thì nó được khởi tạo trong quá trình compile time.
    *       + Runtime:
    *           - Các biến non-static thì được khởi tạo trong quá trình runtime.
     */


    private static final double PI = 3.14;
    private final double a;

    public Main(){
        a = 3;
    }

    public static void main(String[] args) {

        System.out.println(Main.PI);

        Main main = new Main();
        System.out.println(main.a);

        Student student = new Student("CuongLM");
        System.out.println(student);
        Student student1 = new Student("QuynhLT");
        System.out.println(student1);

    }

}
