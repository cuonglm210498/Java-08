package com.lecuong.apply;

import com.lecuong.apply.controller.Controller;
import com.lecuong.apply.controller.ProductController;
import com.lecuong.apply.controller.RoleController;
import com.lecuong.apply.controller.UserController;
import com.lecuong.apply.service.impl.ProductService;
import com.lecuong.apply.service.impl.RoleService;
import com.lecuong.apply.service.impl.UserService;

public class ControllerFactory {

    public static Controller createController(int controllerType, int serviceType){
        switch (controllerType){
            case 1:
                UserService userService = (UserService) ServiceFactory.getService(serviceType);
                return new UserController(userService);
            case 2:
                RoleService roleService = (RoleService) ServiceFactory.getService(serviceType);
                return new RoleController(roleService);
            case 3:
                ProductService productService = (ProductService) ServiceFactory.getService(serviceType);
                return new ProductController(productService);
        }
        return null;
    }
}
