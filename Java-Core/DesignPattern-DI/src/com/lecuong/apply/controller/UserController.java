package com.lecuong.apply.controller;

import com.lecuong.apply.service.impl.UserService;

public class UserController implements Controller {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    public void save(){
        userService.save();
    }

    public void update(){
        userService.update();
    }

}
