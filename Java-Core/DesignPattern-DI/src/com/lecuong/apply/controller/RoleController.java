package com.lecuong.apply.controller;

import com.lecuong.apply.service.impl.RoleService;

public class RoleController implements Controller {

    private RoleService roleService;

    public RoleController(RoleService roleService) {
        this.roleService = roleService;
    }

    public void getAll(){
        roleService.getAll();
    }
}
