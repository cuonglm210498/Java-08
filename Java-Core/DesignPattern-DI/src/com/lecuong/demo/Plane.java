package com.lecuong.demo;

import com.lecuong.demo.ITravel;

public class Plane implements ITravel {

    @Override
    public void move(){
        System.out.println("Travel by plane");
    }
}
