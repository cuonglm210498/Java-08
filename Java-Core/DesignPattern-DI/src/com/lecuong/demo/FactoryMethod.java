package com.lecuong.demo;

public class FactoryMethod {

    public static ITravel create(int type){
        switch (type){
            case 1:
                return new Plane();
            case 2:
                return new Car();
            case 3:
                return new Train();
            case 4:
                return new Bike();
        }
        return null;
    }

}
