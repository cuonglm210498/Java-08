package com.lecuong.demo;

import com.lecuong.demo.ITravel;

public class Travel {

    private ITravel travel;

    public Travel(ITravel travel) {
        this.travel = travel;
    }

    public void travel(){
        travel.move();
    }

}
