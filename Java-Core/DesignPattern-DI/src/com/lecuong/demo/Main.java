package com.lecuong.demo;

public class Main {

    public static void main(String[] args) {

        ITravel iTravel = FactoryMethod.create(1);
        Travel travel = new Travel(iTravel);
        travel.travel();
    }

}
