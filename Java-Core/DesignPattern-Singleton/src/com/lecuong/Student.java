package com.lecuong;

public class Student {

    private long id;
    private String name;
    private static Student student;

    private Student(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public static Student getInstance(long id, String name){
        if (student == null){
            student = new Student(id, name);
        }
        return student;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
