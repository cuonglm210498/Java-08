package com.lecuong;

public class Main {

    public static void main(String[] args) {

        Student s1 = Student.getInstance(1, "CuongLM");
        Student s2 = Student.getInstance(2, "QuynhLT");

        System.out.println(s1.toString());
        System.out.println(s2.toString());

//        Student{id=1, name='CuongLM'}
//        Student{id=1, name='CuongLM'}

    }

}
