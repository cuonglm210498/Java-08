package com.lecuong;

public class Main<T> {

    /*
    * Generic:
    *       + được ra đời từ java 5.
    *       + nó là đại diện một kiểu dữ liệu nào đó mà ta chưa biết tại thời điểm code.
    *
    */

    public T newIncrement(Class<T> tClass) throws IllegalAccessException, InstantiationException {
        return tClass.newInstance();
    }

    public static <P> P demo(P p){
        return p;
    }

    public static void main(String[] args) {
        Main<Student> create = new Main<>();
        try {
            Student student = create.newIncrement(Student.class);
            student.setName("cuonglm");
            System.out.println(student.getName());
        } catch (IllegalAccessException | InstantiationException e) {
            e.printStackTrace();
        }
    }

}
