package com.lecuong;

public class QueryBuilder {
    private String name;
    private String address;
    private String fromAge;
    private String toAge;
    private String email;
    private String phone;

    private QueryBuilder(Builder builder) {
        this.name = builder.name;
        this.address = builder.address;
        this.fromAge = builder.fromAge;
        this.toAge = builder.toAge;
        this.email = builder.email;
        this.phone = builder.phone;
    }

    public static class Builder {
        private String name;
        private String address;
        private String fromAge;
        private String toAge;
        private String email;
        private String phone;

        public Builder setName(String name) {
            this.name = name;
            return this;
        }

        public Builder setAddress(String address) {
            this.address = address;
            return this;
        }

        public Builder setFromAge(String fromAge) {
            this.fromAge = fromAge;
            return this;
        }

        public Builder setToAge(String toAge) {
            this.toAge = toAge;
            return this;
        }

        public Builder setEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder setPhone(String phone) {
            this.phone = phone;
            return this;
        }

        public QueryBuilder build(){
            return new QueryBuilder(this);
        }
    }

    @Override
    public String toString() {
        return "QueryBuilder{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", fromAge='" + fromAge + '\'' +
                ", toAge='" + toAge + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
