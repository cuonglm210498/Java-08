package com.lecuong;

public class Main {

    // Design Builder được sử dụng khi một class nó có nhiều constructor

    public static void main(String[] args) {
        QueryBuilder queryBuilder = new QueryBuilder.Builder()
                .setAddress("Nam Định")
                .setEmail("Email")
                .setPhone("12345678910")
                .build();

        System.out.println(queryBuilder.toString());
    }
}
