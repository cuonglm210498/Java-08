package com.lecuong.override;

public class Child extends PerSon {

    String email;

    public Child() {
    }

    public Child(String name, int age, String address, String email) {
        super(name, age, address);
        this.email = email;
    }

    @Override
    public void nhap() {
        super.nhap();
        System.out.println("Nhap email: ");
        email = sc.nextLine();
    }

    @Override
    public void hien() {
        super.hien();
        System.out.println("\tEmail: " + email);
    }
}
