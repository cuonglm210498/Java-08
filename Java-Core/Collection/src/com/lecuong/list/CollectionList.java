package com.lecuong.list;

import java.util.*;
import java.util.function.Consumer;

public class CollectionList {

    public static void main(String[] args) {

        List<Integer> listNumberInteger = new ArrayList<>();
        listNumberInteger.add(1);
        listNumberInteger.add(5);
        listNumberInteger.add(9);
        listNumberInteger.add(6);
        listNumberInteger.add(8);
        listNumberInteger.add(10);

        //Cach 1:

        Consumer<Integer> consumer = new Consumer<Integer>() {
            @Override
            public void accept(Integer integer) {
                System.out.print(integer);
            }
        };
        listNumberInteger.forEach(consumer);
        System.out.println();

        listNumberInteger.forEach(p -> System.out.print(p));
        System.out.println();

        //Cach 2:
        for (int i = 0; i < listNumberInteger.size(); i++) {
            System.out.print(listNumberInteger.get(i));
        }
        System.out.println();

        //Cach 3:
        for (Integer ele : listNumberInteger) {
            System.out.print(ele);
        }
        System.out.println();

        //Cach 4:
        Iterator<Integer> i = listNumberInteger.iterator();
        while (i.hasNext()) {
            System.out.print(i.next());
        }
        System.out.println();

        //Cach 5:
        listNumberInteger.forEach(System.out::print);
        System.out.println();

        //Sap xep tang dan
        listNumberInteger.sort((a,b) -> a.compareTo(b));
        System.out.print(listNumberInteger);
        System.out.println();

        //Sap xep giam dan
        listNumberInteger.sort((a,b) -> b.compareTo(a));
        System.out.print(listNumberInteger);
        System.out.println();

        //Sap xep
        Collections.sort(listNumberInteger, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer o2) {
                return o1 > o2 ? 1 : -1;
            }
        });
        System.out.print(listNumberInteger);
        System.out.println();

        //Ham lay ra so luong phan tu trong list
        System.out.println(listNumberInteger.size());

        //Ham kiem tra xem co gia tri trong list hay khong
        System.out.println(listNumberInteger.contains(1));

        //Ham kiem tra xem cac phan tu trong list khac co nam trong list nay hay ko
        List<Integer> listInteger = new ArrayList<>();
        listInteger.add(1);
        listInteger.add(6);
        System.out.println(listNumberInteger.containsAll(listInteger));

        //Ham kiem tra rong
        System.out.println(listNumberInteger.isEmpty());

        //Ham tra ve vi tri cuoi cung cua phan tu
        System.out.println(listNumberInteger.lastIndexOf(5));

    }

}
