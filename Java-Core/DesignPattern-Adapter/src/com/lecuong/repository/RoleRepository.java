package com.lecuong.repository;

import com.lecuong.entity.Role;

public interface RoleRepository extends Repository<Role, Long> {
}
