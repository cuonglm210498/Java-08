package com.lecuong.repository;

import com.lecuong.entity.Product;

public interface ProductRepository extends Repository<Product, Long> {
}
