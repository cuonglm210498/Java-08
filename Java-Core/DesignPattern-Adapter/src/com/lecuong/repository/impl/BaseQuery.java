package com.lecuong.repository.impl;

import com.lecuong.repository.Repository;
import java.lang.reflect.ParameterizedType;

public class BaseQuery<T, ID> implements Repository<T, ID> {

    Class<T> tClass;

    public BaseQuery(){
        tClass = (Class<T>) ((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public void save(T t) {
        System.out.println("Save: " + tClass.getSimpleName() +"---" + t);
    }

    @Override
    public void update(ID id) {
        System.out.println("Update: " + tClass.getSimpleName() + "---" + id);
    }

    @Override
    public void delete(ID id) {
        System.out.println("Delete: " + tClass.getSimpleName() + "---" + id);
    }
}
