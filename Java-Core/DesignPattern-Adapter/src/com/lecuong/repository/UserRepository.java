package com.lecuong.repository;

import com.lecuong.entity.User;
import com.lecuong.repository.Repository;

public interface UserRepository extends Repository<User, Long> {
}
