package com.lecuong.repository;

public interface Repository<T, ID> {

    void save(T t);

    void update(ID id);

    void delete(ID id);

}
