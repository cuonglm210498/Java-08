package com.lecuong;

public class Main {

    public static void main(String[] args) {

        /*
        * Abstract class:
        *       + là một class trừu tượng.
        *       + bản thân nó là một bản thiết kế
        *       + trên lý thuyết chúng ta không thể tạo 1 object từ abstract class, thực ra chúng ta có thể tạo 1 object từ abstract class qua lớp giả.
        *       + được sinh ra để làm cha của những thằng khác. Nó vẫn có thể extends từ thằng khác và thằng khác extends nó.
        *       +
        *
        * Has-a: là trong 1 class chứa đối tượng của class khác.
        * Is-a: là quan hệ cha con. VD: Cat is-a Animal.
        * Khi thiết kế class thì người ta thường ko hướng đến mqh is-a. Mà ngta thường hướng đến mqh has-a.
        */

        Animal cat = new Cat();
        cat.speak();

    }

}
