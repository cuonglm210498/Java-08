package com.lecuong.shop.service.impl;

import com.lecuong.shop.entity.User;
import com.lecuong.shop.repository.UserRepository;
import com.lecuong.shop.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends AbstractServiceImpl<User, Long> implements UserService {

    public UserServiceImpl(UserRepository userRepository) {
        super(userRepository);
    }
}
