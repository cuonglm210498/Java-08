package com.lecuong.shop.service;

import com.lecuong.shop.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService extends BaseService<User, Long> {
}
