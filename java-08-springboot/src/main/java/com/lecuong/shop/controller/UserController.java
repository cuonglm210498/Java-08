package com.lecuong.shop.controller;

import com.lecuong.shop.constant.annotation.PermissionData;
import com.lecuong.shop.entity.Role;
import com.lecuong.shop.entity.User;
import com.lecuong.shop.mapper.UserMapper;
import com.lecuong.shop.model.request.user.UserAuthRequest;
import com.lecuong.shop.model.request.user.UserFilterRequest;
import com.lecuong.shop.model.request.user.UserSaveRequest;
import com.lecuong.shop.model.response.BaseResponse;
import com.lecuong.shop.model.response.PageResponse;
import com.lecuong.shop.model.response.user.UserResponse;
import com.lecuong.shop.repository.specification.UserSpecification;
import com.lecuong.shop.security.jwt.TokenProducer;
import com.lecuong.shop.security.jwt.model.JWTPayload;
import com.lecuong.shop.service.UserService;
import com.lecuong.shop.utils.PasswordHasher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.MessageFormat;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController {

    private final static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private TokenProducer tokenProducer;

    @PostMapping("/auth")
    public ResponseEntity<BaseResponse> auth(@RequestBody UserAuthRequest userAuthRequest){

        //Ghi log
        LOGGER.info(MessageFormat.format("request body: {0}" ,userAuthRequest.toString()));

        String password = PasswordHasher.hash(userAuthRequest.getPassword());
        userAuthRequest.setPassword(password);
        User user = userService.findOne(UserSpecification.authFilter(userAuthRequest));
        JWTPayload jwtPayload = creatPayload(user);

        return success(tokenProducer.token(jwtPayload));
    }

    @PostMapping
    @PermissionData
    public ResponseEntity<BaseResponse> save(@RequestBody UserSaveRequest userSaveRequest) {
        userService.save(userSaveRequest, userMapper::mapToEntity);
        return success();
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<BaseResponse> delete(@PathVariable long id) {
        userService.delete(id);
        return success();
    }


    @GetMapping("/{id}")
    public ResponseEntity<BaseResponse> findById(@PathVariable long id) {
        return success(userService.findById(id, userMapper::mapToResponse));
    }

    @GetMapping
    public ResponseEntity<BaseResponse> findAll(@ModelAttribute UserFilterRequest userFilterRequest) {
        PageRequest pageRequest = PageRequest.of(userFilterRequest.getIndex(), userFilterRequest.getSize(), Sort.by("id").descending());
        return success(userService.find(UserSpecification.filter(userFilterRequest),pageRequest, userMapper::mapToResponse));
    }

    private JWTPayload creatPayload(User user){
        JWTPayload jwtPayload = new JWTPayload();
        jwtPayload.setEmail(user.getEmail());
        jwtPayload.setUserName(user.getUserName());
        jwtPayload.setId(user.getId());
        String role = user.getRoles().stream().map(Role::getName).collect(Collectors.joining(","));
        jwtPayload.setRole(role);

        return jwtPayload;
    }
}
