package com.lecuong.shop.mapper;

import com.lecuong.shop.entity.Role;
import com.lecuong.shop.entity.User;
import com.lecuong.shop.model.request.user.UserSaveRequest;
import com.lecuong.shop.model.response.user.UserResponse;
import com.lecuong.shop.repository.RoleRepository;
import com.lecuong.shop.utils.BeanUtils;
import com.lecuong.shop.utils.PasswordHasher;
import com.lecuong.shop.validate.UserValidate;
import lombok.Data;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.List;

@Component
@Data
public class UserMapper {

    private final RoleRepository roleRepository;

    public User mapToEntity(UserSaveRequest userSaveRequest) {
        UserValidate.validate(userSaveRequest);
        User user = new User();
        BeanUtils.refine(userSaveRequest, user, BeanUtils::copyNonNull);
        List<Role> roles = roleRepository.findAllByIdIn(userSaveRequest.getIds());
        String password = PasswordHasher.hash(userSaveRequest.getPassword());
        user.setPassword(password);
        user.setRoles(new HashSet<>(roles));
        return user;
    }

    public UserResponse mapToResponse(User user) {
        UserResponse userResponse = new UserResponse();
        BeanUtils.refine(user, userResponse, BeanUtils::copyNonNull);
        return userResponse;
    }
}
