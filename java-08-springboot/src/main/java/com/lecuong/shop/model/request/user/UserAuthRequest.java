package com.lecuong.shop.model.request.user;

import lombok.Data;

@Data
public class UserAuthRequest {
    private String userName;
    private String password;
}
