package com.lecuong.shop.model.request.user;

import com.lecuong.shop.model.request.BaseRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserFilterRequest extends BaseRequest {
    private String userName;
    private String password;
    private String address;
}
