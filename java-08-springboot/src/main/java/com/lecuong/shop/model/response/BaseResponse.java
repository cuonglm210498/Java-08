package com.lecuong.shop.model.response;

import com.lecuong.shop.exception.ResponseStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseResponse {

    private ResponseStatus status;
    private Object baseData;

    public BaseResponse(ResponseStatus status, Object baseData) {
        this.status = status;
        this.baseData = baseData;
    }

    public BaseResponse(ResponseStatus status) {
        this.status = status;
        this.baseData = null;
    }
}
