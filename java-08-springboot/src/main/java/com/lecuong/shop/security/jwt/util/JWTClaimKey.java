package com.lecuong.shop.security.jwt.util;

public enum JWTClaimKey {

    ID("id"),
    EMAIL("email"),
    USERNAME("userName"),
    ROLE("role");

    private String value;

    private JWTClaimKey(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
