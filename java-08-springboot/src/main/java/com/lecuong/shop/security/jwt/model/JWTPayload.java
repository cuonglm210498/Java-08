package com.lecuong.shop.security.jwt.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class JWTPayload {

    private long id;
    private String userName;
    private String email;
    private String role;
}
