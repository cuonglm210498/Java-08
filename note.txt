SPRING BOOT JAVA-08

Buổi 1:
- Lý thuyết:
	+ Spring là một framework hỗ trợ đầy đủ để chúng ta phát triển phần mềm một cách nhanh chóng.
	+ Bean: là các Object mà chúng ta cần sử dụng trong suốt chương trình. VD: UserService, UserRepository,...
	+ BeanFactory: là nơi lưu trữ các Bean.
	+ IOC (Inversion Of Control): Nghịch đảo điều khiển.
	+ DI (Dependence Injection): Tiêm phụ thuộc.
- Cấu hình file application.properties.
	+ server.port=8080
	+ spring.datasource.url=jdbc:mysql://localhost:3306/shop
	+ spring.datasource.username=root
	+ spring.datasource.password=
	+ spring.datasource.driver-class-name=com.mysql.jdbc.Driver
	+ spring.jpa.generate-ddl=true
	+ spring.jpa.show-sql=true
- Tạo Package:
	+ entity
	+ repository
	+ service
	+ controller
- Tạo các entity:
	+ Các annotation cần biết: @Entity, @Id, @Column, @OneToMany, @ManyToOne, @OneToOne, @ManyToMany, @JoinTable, @JoinColumn,...
	+ Tạo BaseEntity, EntityAuditing, và các entity khác,...
	+ Tạo package repository, UserRepository,...
	+ Tạo package service, UserService,...
	+ Tạo package controller, UserController,...

Buổi 2:

- Lý thuyết:

- Thực hành:
	+ Tạo interface BaseRepository, các repository khác extends BaseRepository.
	+ Tạo package model(request, response)
	+ Tạo interface BaseService, PageResponse, AbstractServiceImpl.
	+ Sửa lại UserService, UserServiceImpl
	+ Tạo class UserSaveRequest.
	+ Tạo package utils và tạo class BeanUtils.
	+ Tạo interface RoleRepository.
	+ Tạo package mapper, tạo class UserMapper.
	+ Tạo class UserResponse, PageResponse, BaseRequest, UserFilterRequest.
	+ Add thư viện commons-lang3.
	+ Tạo package specification trong package repository, tạo class UserSpecification.
	+ Tạo package validate, tạo class Validator, UserValidate.
	+ Tạo package exception, tạo class ValidateException, ObjectNotFoundException, ResponseStatus, NotiTemplate.
	+ Tạo class BaseResponse.
	+ Tạo BaseController